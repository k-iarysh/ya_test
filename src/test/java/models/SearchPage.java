package models;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage extends BasePage {
    public SearchPage(WebDriver wd, WebDriverWait wait) {
        super(wd, wait);
    }

    public ProductPage searchProduct(String phoneToFind) {
        wd.findElement(Locators.searchField).sendKeys(phoneToFind);
        click(Locators.searchButton);
        return new ProductPage(wd, wait);
    }
//    public MobilePhonesPage searchPhone() {
//
//    }
}
