package models;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class FilterPage extends BasePage {
    FilterPage(WebDriver wd, WebDriverWait wait) {
        super(wd, wait);
    }


    public FilterPage setManufacturer(String manufacturer) {
        wait.until(ExpectedConditions.presenceOfElementLocated(Locators.manufacturerBox));

        List<WebElement> manList = this.wd.findElements(Locators.manufacturers);

        for (WebElement man : manList) {
            if (man.getText().contains(manufacturer)) {
                man.click();
                break;
            }
        }
        return this;

    }

    public FilterPage setMinPrice(String minPrice) {
        wd.findElement(Locators.inputPriceFrom).sendKeys(minPrice);
        return this;
    }


    public ProductPage pressFindProducts() {
        this.click(Locators.find);
        return new ProductPage(wd, wait);
    }
}
