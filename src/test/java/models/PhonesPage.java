package models;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class PhonesPage extends ProductPage {
    public PhonesPage(WebDriver wd, WebDriverWait wait) {
        super(wd, wait);
    }

    //    Get count of phones per page after filtering
    @Override
    public int getProductPerPage() {
        List<WebElement> we = wait.until(ExpectedConditions.
                presenceOfAllElementsLocatedBy(Locators.foundElements));
        return we.size();
    }

    @Override
    public String getFirstElementName() {
        List<WebElement> we = wait.until(ExpectedConditions.
                presenceOfAllElementsLocatedBy(Locators.foundElements));
        if (we.size() > 0) {
            return we.get(0).getText();
        }
        return "";
    }
}
