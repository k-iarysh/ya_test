package models;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static locators.Locators.electronics;
import static locators.Locators.market;

public class MarketPage extends BasePage {
    public MarketPage(WebDriver wd, WebDriverWait wait) {
        super(wd, wait);
    }

    public MarketPage init() {
        wd.get("https://www.ya.ru/");
        wd.get("https://www.yandex.ru/");
        wd.findElement(market).click();
        return this;
    }

    public ElectronicPage goToElectronicPage() {
        click(electronics);
//        wd.get("https://market.yandex.ru/catalog--elektronika/54440");
        return new ElectronicPage(wd, wait);
    }

    public ProductPage goToMobilePage(){
        wd.get("https://market.yandex.ru/catalog--mobilnye-telefony-v-ekaterinburge/54726/");
        return new ProductPage(wd, wait);
    }


    public FilterPage goToFilterPage() {
        wd.get("https://market.yandex.ru/catalog/54726/filters?onstock=1&deliveryincluded=0&local-offers-first=0");
        return new FilterPage(wd, wait);
    }
}
