package models;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static locators.Locators.filterButton;

public class ProductPage extends BasePage {
    ProductPage(WebDriver wd, WebDriverWait wait) {
        super(wd, wait);
    }

    // makes float price from string
    public float getFloatPrice(String str) {
        str = str.replace(" ", "");
        Pattern pattern = Pattern.compile("\\d*");
        Matcher matcher = pattern.matcher(str);
        float f = 0;
        if (matcher.find()) {
            f = Float.parseFloat(str.substring(matcher.start(), matcher.end()));
            System.out.println(f);
        }
        return f;
    }


    public FilterPage goToFilterPage() {
        click(filterButton);
        return new FilterPage(wd, wait);
    }

    public ProductPage setSortOrderByPrice() {
        click(Locators.sortPrice);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    //    get prices from product page
    public List<Float> getPrices() {
        List<WebElement> priceWebElements = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(Locators.price));
        List<String> rowPrices = priceWebElements.stream().map(WebElement::getText).collect(Collectors.toList());
        return rowPrices.stream().map(this::getFloatPrice).collect(Collectors.toList());
    }

    public int getProductPerPage() {
        List<WebElement> we = wait.until(ExpectedConditions.
                presenceOfAllElementsLocatedBy(Locators.foundElements));
        return we.size();
    }

    public String getFirstElementName() {
        List<WebElement> we = wait.until(ExpectedConditions.
                presenceOfAllElementsLocatedBy(Locators.foundElements));
        if (we.size() > 0) {
            return we.get(0).getText();
        }
        return "";
    }

    ;

}
