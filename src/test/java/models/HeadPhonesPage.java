package models;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class HeadPhonesPage extends ProductPage {
    HeadPhonesPage(WebDriver wd, WebDriverWait wait) {
        super(wd, wait);
    }

    //    Get count of phones per page after filtering
    @Override
    public int getProductPerPage() {
        List<WebElement> we = wait.until(ExpectedConditions.
                presenceOfAllElementsLocatedBy(Locators.foundElementsHeadPhones));
        return we.size();
    }

    //    Get first element caption
    @Override
    public String getFirstElementName() {
        List<WebElement> we = wait.until(ExpectedConditions.
                presenceOfAllElementsLocatedBy(Locators.foundElements));
        if (we.size() > 0) {
            return we.get(0).getText();
        }
        return "";
    }



}
