package models;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static locators.Locators.*;

public class ElectronicPage extends BasePage {
    ElectronicPage(WebDriver wd, WebDriverWait wait) {
        super(wd, wait);
    }
    public ProductPage goToMobilePhone() {
        click(mobilePhone);
        return new ProductPage(wd, wait);
    }

    public ProductPage goToHeadPhone() {
        click(headPhone);
        return new ProductPage(wd, wait);
    }

}
