import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

public class TestString {
    @Test
    public void test(){
        String str = "16 369 ₽";
        str = str.replace(" ", "");
        Pattern pattern = Pattern.compile("\\d*");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            float f = Float.parseFloat(str.substring(matcher.start(), matcher.end()));
            System.out.println(f);
        }
    }


    @Test
    public void tst1(){
        List<Float> actualPrices = new ArrayList<>();
        actualPrices.add((float) 1.0);
        actualPrices.add((float) 7.0);
        actualPrices.add((float) 8.0);
        List<Float> sortedPrices = new ArrayList<>(actualPrices);
        Collections.sort(sortedPrices);
        System.out.println(actualPrices);
        System.out.println(sortedPrices);
        assertEquals(actualPrices, sortedPrices);

    }
}
