import models.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class YandexTest extends BaseTest {
    int expectedProductCount = 12;
    private static final Logger logger = LoggerFactory.getLogger(YandexTest.class);

    @Test
    public void testPhones() {
        logger.info("Testing phone");
        FilterPage filterPage = new MarketPage(driver, wait).init().goToElectronicPage().goToMobilePhone().goToFilterPage();
        ProductPage foundPhonesPage = filterPage
                .setManufacturer("Samsung")
                .setManufacturer("Apple")
                .setMinPrice("20000")
                .pressFindProducts();
        int foundPhones = foundPhonesPage.getProductPerPage();
        assertEquals(foundPhones, expectedProductCount);
        String firstPhoneName = foundPhonesPage.getFirstElementName();
        logger.info(firstPhoneName);
        ProductPage searchPhonePage = new SearchPage(driver, wait).searchProduct(firstPhoneName);
        String actualFirstPhoneName = searchPhonePage.getFirstElementName();
        assertEquals(actualFirstPhoneName, firstPhoneName);
        logger.info("Testing ended");
    }

    @Test
    public void testHeadPhones() {
        logger.info("Testing headphones");
        FilterPage filterPage = new MarketPage(driver, wait).init().goToElectronicPage().goToHeadPhone().goToFilterPage();
        ProductPage foundHeadPhonesPage = filterPage
                .setManufacturer("Beats")
                .setMinPrice("5000")
                .pressFindProducts();
        int foundHeadPhones = foundHeadPhonesPage.getProductPerPage();
        assertEquals(expectedProductCount, foundHeadPhones);
        String firstHeadPhoneName = foundHeadPhonesPage.getFirstElementName();
        logger.info(firstHeadPhoneName);
        ProductPage searchPhonePage = new SearchPage(driver, wait).searchProduct(firstHeadPhoneName);
        String actualFirstHeadPhoneName = searchPhonePage.getFirstElementName();
        assertEquals(firstHeadPhoneName, actualFirstHeadPhoneName);
        logger.info("Testing ended");
    }


    @Test
    public void testSort() {
        logger.info("Testing sorting");
        ProductPage productPage = new MarketPage(driver, wait)
                .init()
                .goToElectronicPage()
                .goToMobilePhone()
                .setSortOrderByPrice();
        List<Float> actualPrices = productPage.getPrices();
        List<Float> sortedPrices = new ArrayList<>(actualPrices);
        Collections.sort(sortedPrices);
        logger.info(actualPrices.toString());
        logger.info(sortedPrices.toString());
        assertEquals(sortedPrices, actualPrices);
    }



}
