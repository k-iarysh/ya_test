package locators;

import org.openqa.selenium.By;

public class Locators {
    public static By electronics = By.linkText("�����������");
    public static By market = By.linkText("������");
    public static By mobilePhone = By.linkText("��������� ��������");
    public static By headPhone = By.partialLinkText("��������");
    public static By filterButton = By.linkText("��� �������");
    public static By sortPrice = By.linkText("�� ����");
    public static By inputPriceFrom = By.id("glf-pricefrom-var");

    public static By manufacturerBox = By.xpath("//span[contains(text(), \"�������������\")]/..");
    public static By manufacturers = By.cssSelector("div.n-filter-block__list-items.i-bem a label");

    public static By find = By.linkText("�������� ����������");

    public static By foundElements = By.cssSelector("div[class=n-snippet-cell2__header] h3 a");

    public static By foundElementsHeadPhones = By.cssSelector("h3.n-snippet-card2__title a");

    public static By searchField = By.cssSelector("input[id=header-search]");
    public static By searchButton = By.cssSelector("button.button2");

    public static By price = By.cssSelector("div[class=price]");
}
